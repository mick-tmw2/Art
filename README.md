# Art Repository

All stuff which breaks client-data ends up here.

All stuff not being used at the moment ends up here.

All stuff not fit for client-data ends up here too.

Server data code may also end up here if needed.

Please note some files may have different license. Keep aware for that.

[![pipeline status](https://gitlab.com/TMW2/Art/badges/master/pipeline.svg)](https://gitlab.com/TMW2/Art/commits/master)